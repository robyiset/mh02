﻿var sjob_id = "";
var dataApproval, dataLogUser, dataLogAction;

var txt_job_id = $("#txt_job_id").kendoDropDownList({
    optionLabel: "All",
    dataTextField: "job_id",
    dataValueField: "job_id",
    //filter: "contains",
    dataSource: {
        type: "json",
        transport: {
            read: {
                url: $("#urlPath").val() + "/ServiceMonitoring/DD_JOB_ID",
                contentType: "application/json",
                type: "POST",
                cache: false
            }
        },
        schema: {
            data: "Data",
            total: "Total"
        }
    },
    select: function (e) {
        var dataItem = this.dataItem(e.item.index());
        //var dataItem = e.dataItem;
        sjob_id = dataItem.job_id;
        loadgridMasterLog(sjob_id);
    }
});


function loadgridTaskMonitoring() {
    $("#gridTaskMonitoring").empty();
    var existingGrid = $('#gridTaskMonitoring').data('kendoGrid');
    if (existingGrid) {
        existingGrid.destroy();
    }

    var gridTaskMonitoring = $("#gridTaskMonitoring").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: $("#urlPath").val() + "/ServiceMonitoring/AjaxReadTaskMonitoring"
                }
            },
            pageSize: 10000,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            type: "aspnetmvc-ajax",
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "job_id",
                    fields: {
                        job_id: { type: "string", filterable: true, sortable: true, editable: false },
                        run_datetime: { type: "date", filterable: true, sortable: true, editable: false },
                        proces_insecond: { type: "number", filterable: true, sortable: true, editable: false },
                        job_status: { type: "string", filterable: true, sortable: true, editable: false },
                        job_remark: { type: "string", filterable: true, sortable: true, editable: false },
                        active_status: { type: "number", filterable: true, sortable: true, editable: false },
                        duration: { type: "number", filterable: true, sortable: true, editable: false },
                        interval_type: { type: "string", filterable: true, sortable: true, editable: false },

                    }
                }
            }
        },
        filterable: false,
        sortable: false,
        pageable: false,
        resizable: false,
        editable: "popup",
        //toolbar: [
        //     { template: "<button class='k-button' onclick='openPopUp()'><i class='glyphicon glyphicon-plus-sign'></i>&nbsp;&nbsp;Add Task</button>" },
        //],
        columns: [
            {
                title: "No",
                width: "30px",
                template: "#= ++rowNo #",
                filterable: false
            },
            {
                field: "job_id", title: "Job ID", width: "200px"
            },{
                field: "run_datetime", title: "Run Datetime", width: "150px", template: "#= kendo.toString(kendo.parseDate(run_datetime, 'yyyy-MM-dd HH:mm:ss'), 'dd/MM/yyyy HH:mm:ss') #"
            },{
                field: "proces_insecond", title: "Process <br/>in Second", width: "60px"
            },{
                field: "job_status", title: "Job <br/>Status", width: "50px", template: $('#tmplt_job_status').html()
            },{
                field: "job_remark", title: "Job <br/>Remarks", width: "100px"
            }, {
                field: "interval_type", title: "Interval <br/>Type", width: "50px"
            }, {
                field: "duration", title: "Duration", width: "50px"
            }, {
                field: "active_status", title: "Active <br/>Status", width: "50px"
            },{
                title: "Action", width: "50px", template: $('#tmplt_btn_reset').html()
            }
        ],
        dataBinding: function () {
            window.rowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        dataBound: function () {
            var i = 0;
            $("#gridTaskMonitoring tbody tr").each(function () {
                var dataItem = $("#gridTaskMonitoring").data().kendoGrid.dataSource.data()[i];
                if (dataItem.job_status == "ON") {
                    $("#job_status_color" + dataItem.job_id).append("<span class='label bg-success'>" + dataItem.job_status + "</span>");
                    $("#btn_reset" + dataItem.job_id).hide();
                }
                else if (dataItem.job_status == "OFF") {
                    $("#job_status_color" + dataItem.job_id).append("<span class='label bg-danger'>" + dataItem.job_status + "</span>");
                    if (dataItem.active_status == 0)
                        $("#btn_reset" + dataItem.job_id).hide();
                    else
                        $("#btn_reset" + dataItem.job_id).show();
                }
                i++;
            });
        }
    });
}



function loadgridMasterLog(sJobID) {
    $("#gridMasterLog").empty();
    var existingGrid = $('#gridMasterLog').data('kendoGrid');
    if (existingGrid) {
        existingGrid.destroy();
    }

    var gridMasterLog = $("#gridMasterLog").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: $("#urlPath").val() + "/ServiceMonitoring/AjaxReadMasterLog?sJobID=" + sJobID,
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            type: "aspnetmvc-ajax",
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "log_id",
                    fields: {
                        log_id: { type: "string", filterable: true, sortable: true, editable: false },
                        job_id: { type: "string", filterable: true, sortable: true, editable: false },
                        job_status: { type: "string", filterable: true, sortable: true, editable: false },
                        job_remarks: { type: "string", filterable: true, sortable: true, editable: false },
                        log_date: { type: "date", filterable: true, sortable: true, editable: false },
                    }
                }
            }
        },
        height: 400,
        filterable: false,
        sortable: false,
        pageable: true,
        resizable: false,
        editable: "popup",
        pageable: {
            refresh: true,
            buttonCount: 5,
            input: true,
            pageSizes: [5, 10, 20, 50, 100, 1000, 100000],
            info: true,
            messages: {
            }
        },
        columns: [
            {
                title: "No",
                width: "30px",
                template: "#= ++rowNo #",
                filterable: false
            }
            , { field: "job_id", title: "Job ID", width: "150px" }
            , { field: "log_date", title: "Log Date", width: "150px", template: "#= kendo.toString(kendo.parseDate(log_date, 'yyyy-MM-dd HH:mm:ss'), 'dd/MM/yyyy HH:mm:ss') #" }
            , { field: "job_remarks", title: "Job Remarks", width: "450px" }
        ],
        dataBinding: function () {
            window.rowNo = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        }
    });
}

function Reset(jobID) {
    $.ajax({
        url: $("#urlPath").val() + "/ServiceMonitoring/AjaxReset?sJobID=" + jobID,
        dataType: "json",
        contentType: "application/json",
        type: "POST",
        success: function (data) {
            if (data.status == 1) {
                //swal("Success!", data.remarks, "success");
                $("#gridTaskMonitoring").data("kendoGrid").dataSource.read();
            } else {
                swal("Error!", data.remarks, "error");
                $("#gridTaskMonitoring").data("kendoGrid").dataSource.read();
            }
        }
    });
}

function formatDate(date) {
    var monthNames = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni", "Juli",
        "Agustus", "September", "Oktober",
        "November", "Desember"
    ];

    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();

    return day + ' ' + monthNames[monthIndex] + ', ' + hour + ':' + minutes;
}




$(document).ready(function () {
    loadgridTaskMonitoring();
    loadgridMasterLog(sjob_id);
    

    setInterval(function () { $("#gridTaskMonitoring").data("kendoGrid").dataSource.read(); }, 5000);
    setInterval(function () { $("#gridMasterLog").data("kendoGrid").dataSource.read(); }, 5000);
});