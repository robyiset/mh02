﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MH02.Models
{
    [Table("vw_log_pelanggaran", Schema = "dbo")]
    public class vw_log_pelanggaran
    {
        [Key]
        public string incident_start { get; set; }
        public string incident_date { get; set; }
        public string incident_time { get; set; }
        public string incident_code { get; set; }
        public string frequency_incident { get; set; }
        public string unit_no { get; set; }
        public string road_segment_id { get; set; }
    }
}
