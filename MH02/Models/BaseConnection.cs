﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Models
{
    public class BaseConnection
    {
        static public IConfigurationRoot Configuration { get; set; }
        public BaseConnection()
        {
            var builder = new ConfigurationBuilder()
                         .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        public string MH02Connection()
        {
            return Configuration["MH02Connection"];
        }
    }
}
