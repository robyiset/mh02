﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Models
{
    [Table("tbl_t_message", Schema = "dbo")]
    public class tbl_t_message
    {
        public int id { get; set; }
        public string message { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_date { get; set; }
        public string? modified_by { get; set; }
        public int id_monitor { get; set; }
        public string incident_code { get; set; }
        public string unit_no { get; set; }
    }
}
