﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MH02.Models
{
	[Table("tbl_t_monitor", Schema = "dbo")]
	public class tbl_t_monitor
	{
		[Key]
		public int id { get; set; }
		public double? heart_beat { get; set; }
		public string timestamp { get; set; }
		public string device_id { get; set; }
		public string? device_ip { get; set; }
		public string unit_no { get; set; }
		public string incident_code { get; set; }
		public string incident_start { get; set; }
		public string? incident_end { get; set; }
		public int is_visual_alert_on { get; set; }
		public int is_sound_alert_on { get; set; }
		public string? image_file_name { get; set; }
		public string? video_file_name { get; set; }
		public double? gps_lat { get; set; }
		public double? gps_long { get; set; }
		public string road_segment_id { get; set; }
		public double? hm { get; set; }
		public double? rpm { get; set; }
		public double vehicle_speed { get; set; }
		public DateTime created_date { get; set; }
		public string created_by { get; set; }
		public DateTime? modified_date { get; set; }
		public string? modified_by { get; set; }
	}
}
