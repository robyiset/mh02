﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Models
{
    [Table("vw_sum_confirmation", Schema = "dbo")]
    public class vw_sum_confirmation
    {
        [Key]
        public string confirmation { get; set; }
        public int total { get; set; }
    }
}
