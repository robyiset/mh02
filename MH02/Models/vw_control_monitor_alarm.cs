﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Models
{
    [Table("vw_control_monitor_alarm", Schema = "dbo")]
    public class vw_control_monitor_alarm
    {
        public int id { get; set; }
        public string unit_no { get; set; }
        public string organisation { get; set; }
        public string device_id { get; set; }
        public double vehicle_speed { get; set; }
        public string frequency_incident { get; set; }
        public string incident_start { get; set; }
        public string incident_code { get; set; }
        public int total_alarm { get; set; }
        public string road_segment_id { get; set; }
        public string confirmation { get; set; }
    }
}
