﻿using System;
using Microsoft.EntityFrameworkCore;

namespace MH02.Models
{
    public class MH02Context : DbContext
    {
        public MH02Context()
        {
        }
        public MH02Context(DbContextOptions<MH02Context> options) : base(options)
        {
            Database.SetCommandTimeout((int)TimeSpan.FromMinutes(1).TotalSeconds);
        }

        #region transaction
        public DbSet<tbl_t_monitor> tbl_t_monitor { get; set; }
        public DbSet<tbl_t_confirmation> tbl_t_confirmation { get; set; }
        public DbSet<tbl_t_message> tbl_t_message { get; set; }
        #endregion

        #region view
        public DbSet<vw_control_monitor_alarm> vw_control_monitor_alarm { get; set; }
        public DbSet<vw_log_pelanggaran> vw_log_pelanggaran { get; set; }
        public DbSet<vw_sum_confirmation> vw_sum_confirmation { get; set; }
        public DbSet<vw_sum_incident_code> vw_sum_incident_code { get; set; }
        public DbSet<vw_sum_incident_date> vw_sum_incident_date { get; set; }
        #endregion


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(new BaseConnection().MH02Connection());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        
    }
}
