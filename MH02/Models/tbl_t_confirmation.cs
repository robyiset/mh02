﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MH02.Models
{
    [Table("tbl_t_confirmation", Schema = "dbo")]
    public class tbl_t_confirmation
    {
        [Key]
        public int id { get; set; }
        public string confirmation { get; set; }
        public string catatan { get; set; }
        public string nama_nrp_gl { get; set; }
        public DateTime created_date { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_date { get; set; }
        public string modified_by { get; set; }
        public int id_monitor { get; set; }
    }
}
