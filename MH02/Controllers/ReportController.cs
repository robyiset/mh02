﻿using Kendo.DynamicLinqCore;
using MH02.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Controllers
{
    public class ReportController : Controller
    {
        private MH02Context ctx_db = new MH02Context();
        private IHostingEnvironment environment;
        public ReportController(MH02Context ctx, IHostingEnvironment _environment)
        {
            ctx_db = ctx;
            environment = _environment;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("PNRP") == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewBag.report = "active";
            return View();
        }

        [HttpPost]
        public JsonResult ajaxRead_sum_confirmation([FromBody] DataSourceRequest req)
        {
            var data = ctx_db.vw_sum_confirmation;
            return Json(data.ToDataSourceResult(req));
        }

        [HttpPost]
        public JsonResult ajaxRead_sum_code([FromBody] DataSourceRequest req)
        {
            var data = ctx_db.vw_sum_incident_code;
            return Json(data.ToDataSourceResult(req));
        }

        [HttpPost]
        public JsonResult ajaxRead_sum_date([FromBody] DataSourceRequest req)
        {
            var data = ctx_db.vw_sum_incident_date;
            return Json(data.ToDataSourceResult(req));
        }
    }
}
