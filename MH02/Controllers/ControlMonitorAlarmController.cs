﻿using Kendo.DynamicLinqCore;
using MH02.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Controllers
{
    public class ControlMonitorAlarmController : Controller
    {
        private MH02Context ctx_db;

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("user") != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public JsonResult GetUnit([FromBody] DataSourceRequest req)
        {
            DbSet<vw_control_monitor_alarm> data = null;
            string i_str_unit_no = string.Empty;
            string i_str_frecuency = string.Empty;
            foreach (var item in ctx_db.tbl_t_monitor.OrderBy(i => i.unit_no))
            {
                if (string.IsNullOrEmpty(i_str_unit_no) || item.unit_no != i_str_unit_no || (item.unit_no != i_str_unit_no && item.incident_code == "MICROSLEEP"))
                {
                    i_str_unit_no = item.unit_no;
                    i_str_frecuency = "kesatu";
                }
                else
                {
                    
                    if (item.incident_code == "YAWN" || item.incident_code == "CLE" || item.incident_code == "FACELOSS" || item.incident_code == "NOFACE")
                    {
                        if (i_str_frecuency == "kesatu")
                        {
                            i_str_frecuency = "kedua";
                        }
                        else if (i_str_frecuency == "kedua")
                        {
                            i_str_frecuency = "ketiga";
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                
                data.Add(new vw_control_monitor_alarm
                {
                    id = item.id,
                    unit_no = item.unit_no,
                    vehicle_speed = item.vehicle_speed,
                    frequency_incident = i_str_frecuency,
                    incident_start = item.incident_start,
                    incident_code = item.incident_code,
                    total_alarm = item.is_visual_alert_on + item.is_sound_alert_on,
                    road_segment_id = item.road_segment_id
                });
            }
            
            return Json(data.OrderByDescending(i => i.incident_start).ToDataSourceResult(req));
        }

    }
}
