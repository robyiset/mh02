﻿using MH02.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("PNRP") != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpGet]
        public ActionResult profile()
        {
            if (HttpContext.Session.GetString("PNRP") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.pnrp = HttpContext.Session.GetString("PNRP");
            return View();
        }

        [HttpPost]
        public ActionResult getuser(string pnrp, string password)
        {
            if (pnrp == "12345678" && password == "123123")
            {
                HttpContext.Session.SetString("PNRP", pnrp);
                
                return RedirectToAction("profile", "Login");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        [HttpPost]
        public ActionResult profileSelect(int idDistrik, int idProfile)
        {
            HttpContext.Session.SetInt32("gpId", idProfile);
            HttpContext.Session.SetInt32("distrik", idDistrik);
            return RedirectToAction("Index", "Home");
        }
    }
}
