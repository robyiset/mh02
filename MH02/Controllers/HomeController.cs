﻿using Kendo.DynamicLinqCore;
using MH02.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MH02.Controllers
{
    public class HomeController : Controller
    {
        private MH02Context ctx_db = new MH02Context();
        private IHostingEnvironment environment;
        public HomeController(MH02Context ctx, IHostingEnvironment _environment)
        {
            ctx_db = ctx;
            environment = _environment;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("PNRP") == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewBag.home = "active";
            return View();
        }

        [HttpPost]
        public JsonResult ajaxRead([FromBody] DataSourceRequest req)
        {
            var data = ctx_db.vw_control_monitor_alarm.OrderByDescending(i => i.incident_start);
            return Json(data.ToDataSourceResult(req));
        }

        [HttpGet]
        public JsonResult AjaxInsert(string device_id, string unit_no, string incident_code, string road_segment_id, double vehicle_speed)
        {
            try
            {
                tbl_t_monitor tbl = new tbl_t_monitor
                {
                    timestamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Replace(" ", ""),
                    device_id = device_id.ToUpper(),
                    unit_no = unit_no.ToUpper(),
                    incident_code = incident_code.ToUpper(),
                    incident_start = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Replace(" ", ""),
                    is_sound_alert_on = 1,
                    is_visual_alert_on = 1,
                    road_segment_id = road_segment_id.ToUpper(),
                    vehicle_speed = vehicle_speed,
                    created_date = DateTime.Now,
                    created_by = HttpContext.Session.GetString("PNRP")
                };
                ctx_db.tbl_t_monitor.Add(tbl);
                ctx_db.SaveChanges();
                ctx_db.Dispose();
                return Json(new { status = true, remarks = "Berhasil, Save Data" });
            }
            catch (Exception e)
            {
                return Json(new { status = true, remarks = e.ToString() });
            }
        }

        [HttpGet]
        public JsonResult AjaxUpdate(int id, string device_id, string unit_no, string incident_code, string road_segment_id, double vehicle_speed)
        {
            try
            {
                var tbl = ctx_db.tbl_t_monitor.Where(i => i.id == id).FirstOrDefault();
                if (tbl != null)
                {
                    tbl.timestamp = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Replace(" ", "");
                    tbl.device_id = device_id.ToUpper();
                    tbl.unit_no = unit_no.ToUpper();
                    tbl.incident_code = incident_code.ToUpper();
                    //tbl.incident_start = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss").Replace("/", "").Replace(":", "").Replace(" ", "");
                    tbl.is_sound_alert_on = 1;
                    tbl.is_visual_alert_on = 1;
                    tbl.road_segment_id = road_segment_id.ToUpper();
                    tbl.vehicle_speed = vehicle_speed;
                    tbl.modified_date = DateTime.Now;
                    tbl.modified_by = HttpContext.Session.GetString("PNRP");
                    ctx_db.tbl_t_monitor.Update(tbl);
                    ctx_db.SaveChanges();
                    ctx_db.Dispose();
                    return Json(new { status = true, remarks = "Berhasil, Update Data" });
                }
                else
                {
                    return Json(new { status = false, remarks = "Data Tidak Ditemukan" });
                }
            }
            catch (Exception e)
            {
                return Json(new { status = true, remarks = e.ToString() });
            }
        }

        [HttpGet]
        public JsonResult AjaxDelete(int id)
        {
            try
            {
                var tbl = ctx_db.tbl_t_monitor.Where(i => i.id == id).FirstOrDefault();
                if (tbl != null)
                {
                    ctx_db.tbl_t_monitor.Remove(tbl);
                    ctx_db.SaveChanges();
                    ctx_db.Dispose();
                    return Json(new { status = true, remarks = "Berhasil, Hapus Data" });
                }
                else
                {
                    return Json(new { status = false, remarks = "Data Tidak Ditemukan" });
                }
            }
            catch (Exception e)
            {
                return Json(new { status = true, remarks = e.ToString() });
            }
        }

        [HttpPost]
        public JsonResult ajaxRead_log_pelanggaran([FromBody] DataSourceRequest req, string unit_no)
        {
            var data = ctx_db.vw_log_pelanggaran.Where(i => i.unit_no == unit_no).OrderByDescending(i => i.incident_start);
            return Json(data.ToDataSourceResult(req));
        }

        [HttpPost]
        public JsonResult ajax_InsertConfirmation(int txt_id_monitor_confirm, string txt_confirmation, string txt_catatan, string txt_nama_nrp_gl)
        {
            tbl_t_confirmation tbl = new tbl_t_confirmation
            {
                id_monitor = txt_id_monitor_confirm,
                confirmation = txt_confirmation,
                catatan = txt_catatan,
                nama_nrp_gl = txt_nama_nrp_gl,
                created_date = DateTime.Now,
                created_by = HttpContext.Session.GetString("PNRP")
            };
            ctx_db.tbl_t_confirmation.Add(tbl);
            ctx_db.SaveChanges();
            ctx_db.Dispose();
            return Json(new { status = true, remarks = "Berhasil, Save Data" });
        }

        [HttpPost]
        public JsonResult ajaxRead_message([FromBody] DataSourceRequest req, int id)
        {
            var data = ctx_db.tbl_t_message.Where(i => i.id_monitor == id).OrderByDescending(i => i.created_date);
            return Json(data.ToDataSourceResult(req));
        }

        [HttpPost]
        public JsonResult ajax_InsertMessage(int txt_id_monitor_msg, string txt_incident_msg, string txt_message, string txt_unit_msg, int rd_input, string slt_message)
        {
            string msg = string.Empty;
            if (rd_input == 1)
            {
                msg = slt_message;
            }
            else
            {
                msg = txt_message;
            }
            tbl_t_message tbl = new tbl_t_message
            {
                id_monitor = txt_id_monitor_msg,
                incident_code = txt_incident_msg,
                message = msg,
                unit_no = txt_unit_msg,
                created_date = DateTime.Now,
                created_by = HttpContext.Session.GetString("PNRP")
            };
            ctx_db.tbl_t_message.Add(tbl);
            ctx_db.SaveChanges();
            ctx_db.Dispose();
            return Json(new { status = true, remarks = "Berhasil, Save Data" });
        }

        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
